package eu.musaproject.controller.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import eu.musaproject.entity.Session;
import eu.musaproject.entity.Sla;
import eu.musaproject.repository.SessionRepository;
import multicloud.SLA.converters.plan2neo;
import multicloud.utilities.NeoDriver;

public class SessionControllerImpl {

	@Autowired 
	SessionRepository sessionRepository;

	public Boolean checkValidity(Session session){
		return true;
	}

	public Boolean store(Session session){
		return true;
	}

	public Boolean close(Session session){
		return true;
	}

	public Boolean reactivate(Session session){
		return true;
	}

	public Boolean putModel(Session session, MultipartFile model){
		try {
			ByteArrayInputStream stream = new   ByteArrayInputStream(model.getBytes());
			String stringModel = IOUtils.toString(stream, "UTF-8");
			NeoDriver.execute(stringModel);

			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public Boolean putPlan(Session session, MultipartFile plan){
		try {
			ByteArrayInputStream stream = new   ByteArrayInputStream(plan.getBytes());
			String stringPlan = IOUtils.toString(stream, "UTF-8");
			plan2neo.deployMACM(stringPlan);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public Boolean addSlaToSession(Long sessionId, String slaUrl){
		Session session = this.sessionRepository.findOne(sessionId);
		if(session != null){
			Sla s = new Sla();
			//TODO: occorre fare il get dello sla dal link in ingresso e salvare il contenuto nello sla
			session.setSla(s);
			this.sessionRepository.save(session);
			return true;
		}else{
			return false;
		}
	}
}
