package eu.musaproject.controller;

import java.util.List;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import eu.musaproject.controller.impl.SessionControllerImpl;
import eu.musaproject.entity.Control;
import eu.musaproject.entity.Plan;
import eu.musaproject.entity.Session;
import eu.musaproject.entity.Sla;
import eu.musaproject.entity.Slo;
import eu.musaproject.model.JsonResponse;
import eu.musaproject.model.ResponseStatus;
import eu.musaproject.repository.SessionRepository;

@RestController
@RequestMapping(path="/sla-composer/sessions")
public class SessionController {

	SessionControllerImpl controllerImpl = new SessionControllerImpl();
	@Autowired 
	SessionRepository sessionRepository;


	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<JsonResponse> getSessions(){
		return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.OK, this.sessionRepository.findAll()));
	}

	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<JsonResponse> putSession(@RequestParam(value="mcappId") String mcappId){
		if(mcappId != null){
			Session s = new Session(mcappId);
			this.sessionRepository.save(s);
			return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.OK, s.getId()));
		}else{
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(ResponseStatus.KO, "Bad Request!"));
		}
	}

	@RequestMapping(path = "/{id}/checkvalidity", method = RequestMethod.GET)
	ResponseEntity<JsonResponse> checkSessionValidity(@PathVariable Long id){
		Session session = this.sessionRepository.findOne(id);
		if(session != null){
			if(controllerImpl.checkValidity(session)){
				return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.OK, "The session is valid!"));
			}else{
				return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.KO, "The session is not valid!"));
			}
		}else{
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JsonResponse(ResponseStatus.KO, "Session not found!"));
		}
	}

	@RequestMapping(path = "/{id}/reactivate", method = RequestMethod.GET)
	ResponseEntity<JsonResponse> reactivateSession(@PathVariable Long id){
		Session session = this.sessionRepository.findOne(id);
		if(session != null){
			if(controllerImpl.reactivate(session)){
				return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.OK, "The session is not reactivated!"));
			}else{
				return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.KO, "The session is reactivated!"));
			}
		}else{
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JsonResponse(ResponseStatus.KO, "Session not found!"));
		}
	}

	@RequestMapping(path = "/{id}/store", method = RequestMethod.GET)
	ResponseEntity<JsonResponse> storeSession(@PathVariable Long id){
		Session session = this.sessionRepository.findOne(id);
		if(session != null){
			if(controllerImpl.store(session)){
				return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.OK, "The session is stored!"));
			}else{
				return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.KO, "The session is not stored!"));
			}
		}else{
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JsonResponse(ResponseStatus.KO, "Session not found!"));
		}
	}

	@RequestMapping(path = "/{id}/close", method = RequestMethod.GET)
	ResponseEntity<JsonResponse> closeSession(@PathVariable Long id){
		Session session = this.sessionRepository.findOne(id);
		if(session != null){
			if(controllerImpl.close(session)){
				return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.OK, "The session is not closed!"));
			}else{
				return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.KO, "The session is closed!"));
			}
		}else{
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JsonResponse(ResponseStatus.KO, "Session not found!"));
		}
	}

	@RequestMapping(path = "/{id}/model", method = RequestMethod.POST)
	ResponseEntity<JsonResponse> addSessionModel(@PathVariable Long id, @RequestParam("model") MultipartFile model, @RequestParam("model_type") String modelType){
		Session session = this.sessionRepository.findOne(id);
		if(session != null){
			if(modelType.toLowerCase().equals("camel") || modelType.toLowerCase().equals("neo")){
				session.setModelType(modelType);
				this.sessionRepository.save(session);
				if(model != null){
					System.out.println("Content type: "+model.getContentType());
					if(controllerImpl.putModel(session, model)){
						return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.OK, "The session model is stored!"));
					}else{
						return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.KO, "The session model is not stored!"));
					}
				}else{
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(ResponseStatus.KO, "Bad Request!"));
				}
			}else{
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(ResponseStatus.KO, "Bad Parameters!"));
			}
		}else{
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JsonResponse(ResponseStatus.KO, "Session not found!"));
		}
	}


	@RequestMapping(path = "/{id}/plan", method = RequestMethod.POST)
	ResponseEntity<JsonResponse> addSessionPlan(@PathVariable Long id, @RequestParam("plan") MultipartFile plan){
		Session session = this.sessionRepository.findOne(id);
		if(session != null){
			if(plan != null){
				if(controllerImpl.putModel(session, plan)){
					return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.OK, "The session plan is stored!"));
				}else{
					return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.KO, "The session plan is not stored!"));
				}
			}else{
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(ResponseStatus.KO, "Bad Request!"));
			}
		}else{
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JsonResponse(ResponseStatus.KO, "Session not found!"));
		}
	}

	@RequestMapping(path = "/{id}/sla", method = RequestMethod.GET)
	Sla getSessionSla(@PathVariable Long id, @QueryParam("type") String type, @QueryParam("component_id") String component_id){
		Session session = this.sessionRepository.findOne(id);
		return session.getSla();
	}

	@RequestMapping(path = "/{id}/sla", method = RequestMethod.POST)
	ResponseEntity<JsonResponse> addSessionSla(@PathVariable Long id, @RequestParam("sla_url") String slaUrl){
		if(controllerImpl.addSlaToSession(id, slaUrl)){
			return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(ResponseStatus.OK, "The session sla is stored!"));
		}else{
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JsonResponse(ResponseStatus.KO, "Session not found!"));
		}
	}

	@RequestMapping(path = "/{id}/sla/controls", method = RequestMethod.GET)
	List<Control> getSessionSlaControls(@PathVariable Long id){
		Session session = this.sessionRepository.findOne(id);
		return session.getSla().getControls();
	}

	@RequestMapping(path = "/{id}/sla/slos", method = RequestMethod.GET)
	List<Slo> getSessionSlaSlos(@PathVariable Long id){
		Session session = this.sessionRepository.findOne(id);
		return session.getSla().getSlos();
	}

	@RequestMapping(path = "/{id}/sla/findResponsible", method = RequestMethod.GET)
	void getSessionResponsible(@PathVariable Long id){
		//TODO
	}

}

