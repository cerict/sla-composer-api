package eu.musaproject.model;

public class JsonResponse {
	private ResponseStatus status;
	private Object data;
	
	public JsonResponse(ResponseStatus status, Object data){
		this.status = status;
		this.data = data;
	}
	
	public ResponseStatus getStatus() {
		return status;
	}
	public void setStatus(ResponseStatus status) {
		this.status = status;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
