package eu.musaproject.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Session {
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String mcAppId;
	
	@Column
	private String modelType;

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="plan_id")
	private Plan plan;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="sla_id")
	private Sla sla;
	
	public Session(){
		
	}
	
	public Session (String mcappId){
		this.setMcAppId(mcappId);
	}
	
	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Sla getSla() {
		return sla;
	}

	public void setSla(Sla sla) {
		this.sla = sla;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getMcAppId() {
		return mcAppId;
	}

	public void setMcAppId(String mcAppId) {
		this.mcAppId = mcAppId;
	}
	
	public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}
	

}
