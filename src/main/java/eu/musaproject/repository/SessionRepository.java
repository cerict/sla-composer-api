package eu.musaproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.musaproject.entity.Session;

public interface SessionRepository extends JpaRepository<Session, Long> {

}
